﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class AndroidFlurry : IFlurry
{
    private readonly AndroidJavaObject mCurrentActivity;
    private readonly AndroidJavaClass mFlurryClass;
    private readonly string apiKey;


    public AndroidFlurry(string apiKey)
    {
        var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        mCurrentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        mFlurryClass = new AndroidJavaClass("com.flurry.android.FlurryAgent");
        this.apiKey = apiKey;
    }

    public void StartSession(bool debugLog = true, bool errorLog = true)
    {
        mFlurryClass.CallStatic("setLogEnabled", debugLog);
        mFlurryClass.CallStatic("setCaptureUncaughtExceptions", errorLog);

        mFlurryClass.CallStatic("onStartSession", mCurrentActivity, apiKey);
    }

    public void EndSession()
    {
        mFlurryClass.CallStatic("onEndSession", mCurrentActivity);
    }

    public void LogEvent(string eventId, bool timed = false)
    {
        mFlurryClass.CallStatic("logEvent", eventId, timed);
    }

    public void LogEvent(string eventId, Dictionary<string, string> parameters, bool timed = false)
    {
        using (var javaHashMap = new AndroidJavaObject("java.util.HashMap"))
        {
            // Call 'put' via the JNI instead of using helper classes to avoid:
            //  "JNI: Init'd AndroidJavaObject with null ptr!"
            IntPtr methodPut = AndroidJNIHelper.GetMethodID(javaHashMap.GetRawClass(), "put",
                "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");

            var args = new object[2];
            foreach (var kvp in parameters)
                using (var k = new AndroidJavaObject("java.lang.String", kvp.Key))
                using (var v = new AndroidJavaObject("java.lang.String", kvp.Value))
                {
                    args[0] = k;
                    args[1] = v;
                    AndroidJNI.CallObjectMethod(javaHashMap.GetRawObject(),
                        methodPut, AndroidJNIHelper.CreateJNIArgArray(args));
                }
            mFlurryClass.CallStatic("logEvent", eventId, javaHashMap, timed);
        }
    }

    public void EndTimedEvent(string eventId)
    {
        mFlurryClass.CallStatic("endTimedEvent", eventId);
    }

    public void SetAge(int age)
    {
        mFlurryClass.CallStatic("setAge", age);
    }

    public void SetUserID(string userId)
    {
        mFlurryClass.CallStatic("setUserID", userId);
    }

    public void SetGender(Gender gender)
    {
        mFlurryClass.CallStatic("setGender", (byte) gender);
    }

    public void LogPageView()
    {
        mFlurryClass.CallStatic("onPageView");
    }

    public void SetSecureTransport(bool useSSL)
    {
        mFlurryClass.CallStatic("setUseHttps", useSSL);
    }
}