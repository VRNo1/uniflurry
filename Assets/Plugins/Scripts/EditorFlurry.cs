﻿using System.Collections.Generic;
using UnityEngine;

public class EditorFlurry : IFlurry
{
    private bool debugEnabled;
    private bool errorLogEnabled;

    public void StartSession(bool debugLog = true, bool errorLog = true)
    {
        debugEnabled = debugLog;
        errorLogEnabled = errorLog;

        if (debugEnabled)
            Debug.Log("[Flurry]Flurry session started. Debug enabled: " + debugLog + ", error log enabled: " + errorLog);
    }

    public void EndSession()
    {
        if (debugEnabled)
            Debug.Log("[Flurry]End flurry session");
    }

    public void LogEvent(string eventId, bool timed = false)
    {
        if (debugEnabled)
            Debug.Log("[Flurry]Log event:" + eventId + ", timed: " + timed);
    }

    public void LogEvent(string eventId, Dictionary<string, string> parameters, bool timed = false)
    {
        if (debugEnabled)
            Debug.Log("[Flurry]Log event with parameters:" + eventId + ", timed: " + timed);
    }

    public void EndTimedEvent(string eventId)
    {
        if (debugEnabled)
            Debug.Log("[Flurry]End timed event: " + eventId);
    }

    public void SetAge(int age)
    {
        if (debugEnabled)
            Debug.Log("[Flurry]Set age:" + age);
    }

    public void SetUserID(string userId)
    {
        if (debugEnabled)
            Debug.Log("[Flurry]Set user ID:" + userId);
    }

    public void SetGender(Gender gender)
    {
        if (debugEnabled)
            Debug.Log("[Flurry]Set gender:" + gender);
    }

    public void LogPageView()
    {
        if (debugEnabled)
            Debug.Log("[Flurry]Log page view:");
    }

    public void SetSecureTransport(bool useSSL)
    {
        if (debugEnabled)
            Debug.Log("[Flurry]Set secure transport: " + useSSL);
    }
}