﻿using System.Collections.Generic;
using UnityEngine;

#region Flurry static

public static class Flurry
{
    public static void LogEvent(string eventId, bool timed = false)
    {
        FlurryInitilizer.Flurry.LogEvent(eventId, timed);
    }

    public static void LogEvent(string eventId, Dictionary<string, string> parameters, bool timed = false)
    {
        FlurryInitilizer.Flurry.LogEvent(eventId, parameters, timed);
    }

    public static void EndTimedEvent(string eventId)
    {
        FlurryInitilizer.Flurry.LogEvent(eventId);
    }

    public static void SetAge(int age)
    {
        FlurryInitilizer.Flurry.SetAge(age);
    }

    public static void SetUserID(string userId)
    {
        FlurryInitilizer.Flurry.SetUserID(userId);
    }

    public static void SetGender(Gender gender)
    {
        FlurryInitilizer.Flurry.SetGender(gender);
    }

    public static void LogPageView()
    {
        FlurryInitilizer.Flurry.LogPageView();
    }

    public static void SetSecureTransport(bool useSSL)
    {
        FlurryInitilizer.Flurry.SetSecureTransport(useSSL);
    }
}

#endregion

#region Initialization

public class FlurryInitilizer : MonoBehaviour
{
    public string androidFlurryKey = "";
    public string iosFlurryKey = "";
    public bool debugLogEnabled = true;
    public bool eventLogEnabled = true;
    public bool errorsLogEnabled = true;
    private static IFlurry flurry;

    public static IFlurry Flurry
    {
        get
        {
            if (flurry == null)
                Debug.LogError("Flurry library isn't initialized yet! Don't call flurry on awake");
            return flurry;
        }
    }


    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
#if UNITY_ANDROID
        flurry = new AndroidFlurry(androidFlurryKey);
#elif UNITY_IPHONE
        flurry = new iOSFlurry(iosFlurryKey);
#else
        flurry = new EditorFlurry();
#endif
        flurry.StartSession(debugLogEnabled, errorsLogEnabled);
    }

    private void OnApplicationQuit()
    {
        flurry.EndSession();
    }
}

#endregion