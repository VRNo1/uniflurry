﻿using System.Collections.Generic;

public interface IFlurry
{
    void StartSession(bool debugLog = true, bool errorLog = true);

    void EndSession();

    void LogEvent(string eventId, bool timed = false);
    void LogEvent(string eventId, Dictionary<string, string> parameters, bool timed = false);
    void EndTimedEvent(string eventId);

    void SetAge(int age);
    void SetUserID(string userId);
    void SetGender(Gender gender);
    void LogPageView();
    void SetSecureTransport(bool useSSL);
}

public enum Gender
{
    Female = 0,
    Male = 1
}