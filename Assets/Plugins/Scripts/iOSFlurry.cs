﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

public class iOSFlurry : IFlurry
{
    private readonly string apiKey;

    public iOSFlurry(string apiKey)
    {
        this.apiKey = apiKey;
    }


    public void StartSession(bool debugLog = true, bool errorLog = true)
    {
        flurryStartSession(apiKey, debugLog, errorLog);
    }

    public void EndSession()
    {
        //do nothing, ios handles it automatically
    }

    public void LogEvent(string eventId, bool timed = false)
    {
        flurryLogEvent(eventId, timed);
    }

    public void LogEvent(string eventId, Dictionary<string, string> parameters, bool timed = false)
    {
        var strBuilder = new StringBuilder();
        foreach (var param in parameters)
            strBuilder.AppendFormat("{0}={1}", param.Key, param.Value);
        flurryLogEventWithParameters(eventId, strBuilder.ToString(), timed);
    }

    public void EndTimedEvent(string eventId)
    {
        flurryEndTimedEvent(eventId);
    }

    public void SetAge(int age)
    {
        flurrySetAge(age);
    }

    public void SetUserID(string userId)
    {
        flurrySetUserID(userId);
    }

    public void SetGender(Gender gender)
    {
        flurrySetGender(gender == Gender.Male);
    }

    public void LogPageView()
    {
        throw new NotImplementedException();
    }

    public void SetSecureTransport(bool useSSL)
    {
        flurrySetSecureTransport(useSSL);
    }

    #region Library imports

    [DllImport("__Internal")]
    private static extern void flurryStartSession(string apiKey, bool debugLog, bool errorLog);

    [DllImport("__Internal")]
    private static extern void flurrySetAge(int age);

    [DllImport("__Internal")]
    private static extern void flurrySetGender(bool male);

    [DllImport("__Internal")]
    private static extern void flurrySetUserID(string userId);

    [DllImport("__Internal")]
    private static extern void flurryLogPageView();

    [DllImport("__Internal")]
    private static extern void flurrySetSecureTransport(bool useSSL);

    [DllImport("__Internal")]
    private static extern void flurryLogEvent(string eventId, bool timed);

    [DllImport("__Internal")]
    private static extern void flurryLogEventWithParameters(string eventId, string parameters, bool timed);

    [DllImport("__Internal")]
    private static extern void flurryEndTimedEvent(string eventId);

    #endregion
}