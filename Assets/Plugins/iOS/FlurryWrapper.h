//
//  FlurryWrapper.h
//  Unity-iPhone
//
//  Created by Rustam Ganeyev on 10/19/13.
//  Copyright (c) 2013 AKA Group. All rights reserved.
//

#ifndef Unity_iPhone_FlurryWrapper_h
#define Unity_iPhone_FlurryWrapper_h

extern "C" {
  void flurryStartSession(unsigned char* apiKey, bool debugLog, bool errorLog);

  void flurrySetAge(int age);
  void flurrySetGender(bool male);
  void flurrySetUserID(unsigned char* userId);
  void flurryLogPageView();
  void flurrySetSecureTransport(bool useSSL);
  
  void flurryLogEvent(unsigned char* eventId, bool timed);
  void flurryLogEventWithParameters(unsigned char* eventId,unsigned char *parameters, bool timed);
  void flurryEndTimedEvent(unsigned char* eventId); 
}

#endif
