//
//  FlurryWrapper.h
//  Unity-iPhone
//
//  Created by Rustam Ganeyev on 10/19/13.
//  Copyright (c) 2013 AKA Group. All rights reserved.
//
#include "Flurry.h"

void flurryStartSession(unsigned char* apiKey, bool debugLog, bool errorLog) {
  [Flurry setLogEnabled:debugLog];
  [Flurry setCrashReportingEnabled:errorLog];
  
  NSString *str = [NSString stringWithUTF8String:apiKey];
  [Flurry startSession:str];
}

void flurrySetAge(int age) {
  [Flurry setAge:age];
}

void flurrySetGender(bool male) {
  NSString *str = male ? @"m" : @"f";
  [Flurry setGender:str];
}

void flurrySetUserID(unsigned char* userId) {
  NSString *str = [NSString stringWithUTF8String:userId];
  [Flurry setUserID:str];
}

void flurryLogPageView() {
  [Flurry logPageView];
}

void flurrySetSecureTransport(bool useSSL) {
  [Flurry setsecureTransportEnabled:useSSL];
}
  
void flurryLogEvent(unsigned char* eventId, bool timed) {
  [Flurry logEvent:[NSString stringWithUTF8String:eventId] timed:timed];
}

void flurryLogEventWithParameters(unsigned char* eventId,unsigned char *parameters, bool timed) {
  NSString *params = [NSString stringWithUTF8String:parameters];
  NSArray *arr = [params componentsSeparatedByString: @"\n"];
  
  NSMutableDictionary *dict = [[[NSMutableDictionary alloc] init] autorelease];
  for(int i=0;i < [arr count]; i++)
  {
    NSString *str1 = [arr objectAtIndex:i];
    NSRange range = [str1 rangeOfString:@"="];
    if (range.location != NSNotFound) {
      NSString *key = [str1 substringToIndex:range.location];
      NSString *val = [str1 substringFromIndex:range.location + 1];
      [dict setObject:val forKey:key];
    }
  }

  [Flurry logEvent:[NSString stringWithUTF8String:eventId] withParameters:dict timed:timed];
}

void flurryEndTimedEvent(unsigned char* eventId) {
  [Flurry endTimedEvent:[NSString stringWithUTF8String:eventId] withParameters:nil]; //with no-params
}