using System.Collections.Generic;
using UnityEngine;

public class FlurryUI : MonoBehaviour
{
    private void OnGUI()
    {
        GUILayout.BeginVertical();

        /*if (GUILayout.Button("Agent Version", GUILayout.Height(60)))
        {
            Debug.Log("Agent version is " + Flurry.GetAgentVersion());
        }*/

        if (GUILayout.Button("Log Event 1", GUILayout.Height(60)))
        {
            Flurry.LogEvent("Event 1");
        }
        if (GUILayout.Button("Log Event 1 with params", GUILayout.Height(60)))
        {
            var kvps = new Dictionary<string, string> {{"GameName", "MyGame"}, {"Level", "Level1"}};
            Flurry.LogEvent("Event 3", kvps);
        }

        if (GUILayout.Button("Timed Log Event 1 with params", GUILayout.Height(60)))
        {
            var kvps = new Dictionary<string, string> {{"GameName", "MyGame"}, {"Level", "Level1"}};
            Flurry.LogEvent("Timer 2", kvps, true);
        }

        if (GUILayout.Button("Timed Event Start", GUILayout.Height(60)))
        {
            var kvps = new Dictionary<string, string> {{"TGameName", "MyGame"}, {"TLevel", "Level1"}};
            Flurry.LogEvent("Timer 2", kvps, true);
        }

        if (GUILayout.Button("Timed Event End", GUILayout.Height(60)))
        {
            Flurry.EndTimedEvent("Timer 2");
        }

        GUILayout.EndVertical();
    }
}